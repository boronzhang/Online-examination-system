package xin.examinationSystem.imp;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.testPaper.TopicSet;
import xin.examinationSystem.account.testPaper.topic.*;
import xin.examinationSystem.dao.topic.ITestPaperMapper;
import xin.examinationSystem.utils.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TestPaperMapperImp {

    /**
     * 返回各科题目的数量
     *
     * @return 各科数量, 返回顺序单选题、多选题、判断题、填空题、问答题，以#隔开: 12#233#23
     */
    static public String queryAllTopic() {
        String result = "";
        ITestPaperMapper iTestPaperMapper = (ITestPaperMapper) Tools.getImp("ITestPaperMapper");
        //单选题数量
        result += iTestPaperMapper.getSingleChoiceNum();
        result += "#";

        //多选题数量
        result += iTestPaperMapper.getMultipleChoiceQuestionNum();
        result += "#";

        //选择题数量
        result += iTestPaperMapper.getJudgeQuestionNum();
        result += "#";

        //填空题数量
        result += iTestPaperMapper.getGapFillingNum();
        result += "#";

        //问答题数量
        result += iTestPaperMapper.getEssayQuestionNum();

        return result;
    }

    /**
     * @param topicNums 各题目的数量 12#12#12#143#13   [单选题/多选题/判断题/填空题/简答题]各题数量
     * @return 返回试卷的JSON格式
     */
    static public String createTestPaperJson(String ip, String topicNums) {

        String[] strEachTopicNum = topicNums.split("#");
        System.out.println(Arrays.toString(strEachTopicNum));
        int[] eachTopicNum = new int[5];
        for (int i = 0; i < 5; i++) {
            eachTopicNum[i] = Integer.parseInt(strEachTopicNum[i]);
        }

        ITestPaperMapper iTestPaperMapper = (ITestPaperMapper) Tools.getImp("ITestPaperMapper");
        TopicSet topicSet = new TopicSet();
        Object[] disorganize = disorganize(ip, iTestPaperMapper.queryAllSingleChoice(), eachTopicNum[0]);
        topicSet.setSingleChoices(Arrays.stream(disorganize).toArray(SingleChoice[]::new));
        Object[] multiples = disorganize(ip, iTestPaperMapper.queryAllMultipleChoiceQuestion(), eachTopicNum[1]);
        topicSet.setMultipleChoiceQuestions(Arrays.stream(multiples).toArray(MultipleChoiceQuestion[]::new));
        Object[] Judges = disorganize(ip, iTestPaperMapper.queryAllJudgeQuestion(), eachTopicNum[2]);
        topicSet.setJudgeQuestions(Arrays.stream(Judges).toArray(JudgeQuestion[]::new));
        Object[] GapFillings = disorganize(ip, iTestPaperMapper.queryAllGapFilling(), eachTopicNum[3]);
        topicSet.setGapFillings(Arrays.stream(GapFillings).toArray(GapFilling[]::new));
        Object[] EssayQuestions = disorganize(ip, iTestPaperMapper.queryAllEssayQuestion(), eachTopicNum[4]);
        topicSet.setEssayQuestions(Arrays.stream(EssayQuestions).toArray(EssayQuestion[]::new));

        return JSON.toJSONString(topicSet);
    }

    static Object[] disorganize(String ip, Object[] arr, int num) {
        String[] t = ip.split("\\.");

        //先把顺序全部打乱
        for (int i = 0; i < 100; i++) {
            int start = (int) (Math.random() * arr.length);
            int end = (int) (Math.random() * arr.length);
            Object temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
        }

        int lowest = 2;

        //防止lowest个人的人题目相同
        int TopicId = Integer.parseInt(t[t.length - 1]) % lowest;

        if (TopicId == 0) {

            Object[] resultSet = Arrays.copyOf(arr, arr.length / 2);

            for (int i = 0; i < 100; i++) {
                int start = (int) (Math.random() * resultSet.length);
                int end = (int) (Math.random() * resultSet.length);
                Object temp = resultSet[start];
                resultSet[start] = resultSet[end];
                resultSet[end] = temp;
            }
            return Arrays.copyOf(resultSet, num);
        }

        Object[] resultSet = new Object[arr.length / 2];
        int j = 0;
        for (int i = arr.length / 2; i < arr.length; i++) {
            resultSet[j] = arr[i];
            if (++j >= resultSet.length) {
                break;
            }
        }

        for (int i = 0; i < 100; i++) {
            int start = (int) (Math.random() * resultSet.length);
            int end = (int) (Math.random() * resultSet.length);
            Object temp = resultSet[start];
            resultSet[start] = resultSet[end];
            resultSet[end] = temp;
        }
        return Arrays.copyOf(resultSet, num);
    }
}
