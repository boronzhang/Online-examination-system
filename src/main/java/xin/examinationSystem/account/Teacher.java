package xin.examinationSystem.account;

import java.io.Serializable;

public class Teacher implements Serializable {
    private Integer teaId;
    private String teaName;
    private String teaAccount;
    private String teaPassword;
    private String teaBriefIntroduction;

    public Teacher() {
    }

    public Integer getTeaId() {
        return teaId;
    }

    public void setTeaId(Integer teaId) {
        this.teaId = teaId;
    }

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    public String getTeaAccount() {
        return teaAccount;
    }

    public void setTeaAccount(String teaAccount) {
        this.teaAccount = teaAccount;
    }

    public String getTeaPassword() {
        return teaPassword;
    }

    public void setTeaPassword(String teaPassword) {
        this.teaPassword = teaPassword;
    }

    public String getTeaBriefIntroduction() {
        return teaBriefIntroduction;
    }

    public void setTeaBriefIntroduction(String teaBriefIntroduction) {
        this.teaBriefIntroduction = teaBriefIntroduction;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teaId=" + teaId +
                ", teaName='" + teaName + '\'' +
                ", teaAccount='" + teaAccount + '\'' +
                ", teaPassword='" + teaPassword + '\'' +
                ", teaBriefIntroduction='" + teaBriefIntroduction + '\'' +
                '}';
    }
}
