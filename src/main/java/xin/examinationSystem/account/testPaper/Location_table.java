package xin.examinationSystem.account.testPaper;

public class Location_table {
    private Integer sId;
    private Integer tId;
    private String ip;

    public Location_table() {
    }

    public Location_table(Integer sId, Integer tId, String ip) {
        this.sId = sId;
        this.tId = tId;
        this.ip = ip;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "location_table{" +
                "sId='" + sId + '\'' +
                ", tId='" + tId + '\'' +
                ", ip='" + ip + '\'' +
                '}';
    }
}
