package xin.examinationSystem.account.testPaper.topic;

/**
 * 多选题类
 */
public class MultipleChoiceQuestion {
    /**
     * 题目编号
     */
    private Integer id;

    /**
     * 题目内容
     */
    private String topic;

    /**
     * 题目答案
     */
    private String answer;

    /**
     * 题目的选项
     */
    private String option;

    public MultipleChoiceQuestion() {
    }

    public MultipleChoiceQuestion(String topic, String answer, String option) {
        this.topic = topic;
        this.answer = answer;
        this.option = option;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "MultipleChoiceQuestion{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", answer='" + answer + '\'' +
                ", option='" + option + '\'' +
                '}';
    }
}
