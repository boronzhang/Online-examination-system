package xin.examinationSystem.account.testPaper;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class StuLookTestPaperSet {
    private Integer tpId;
    private String headline;
    private String subtitle;
    private Date entTime;
    private Integer grade = 0;

    public StuLookTestPaperSet() {
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Date getEntTime() {
        return entTime;
    }

    public void setEntTime(Date entTime) {
        this.entTime = entTime;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "StuLookTestPaperSet{" +
                "tpId=" + tpId +
                ", headline='" + headline + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", entTime=" + entTime +
                ", grade=" + grade +
                '}';
    }
}
