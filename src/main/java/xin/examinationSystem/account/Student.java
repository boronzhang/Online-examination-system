package xin.examinationSystem.account;

import java.io.Serializable;

public class Student implements Serializable {
    private Integer stuId;
    private String stuName;
    private String stuAccount;
    private String stuPassword;
    private String stuBriefIntroduction;

    public Student() {
    }

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuAccount() {
        return stuAccount;
    }

    public void setStuAccount(String stuAccount) {
        this.stuAccount = stuAccount;
    }

    public String getStuPassword() {
        return stuPassword;
    }

    public void setStuPassword(String stuPassword) {
        this.stuPassword = stuPassword;
    }

    public String getStuBriefIntroduction() {
        return stuBriefIntroduction;
    }

    public void setStuBriefIntroduction(String stuBriefIntroduction) {
        this.stuBriefIntroduction = stuBriefIntroduction;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuId=" + stuId +
                ", stuName='" + stuName + '\'' +
                ", stuAccount='" + stuAccount + '\'' +
                ", stuPassword='" + stuPassword + '\'' +
                ", stuBriefIntroduction='" + stuBriefIntroduction + '\'' +
                '}';
    }
}
