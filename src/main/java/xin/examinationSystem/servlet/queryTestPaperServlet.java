package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.account.testPaper.Location_table;
import xin.examinationSystem.account.testPaper.TP_Title;
import xin.examinationSystem.account.testPaper.TestPaper;
import xin.examinationSystem.account.testPaper.TestPaperSet;
import xin.examinationSystem.account.testPaper.topic.*;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.topic.ITestPaperMapper;
import xin.examinationSystem.imp.TestPaperMapperImp;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

@WebServlet("/queryTestPaperServlet")
public class queryTestPaperServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean studentStatus = false;
        boolean teacherStatus = false;
        Student curStudent = new Student();
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageStudent")) {
                HttpSession session = request.getSession();
                curStudent = (Student) session.getAttribute(tCookie.getValue());
                if (curStudent != null) {
                    studentStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!studentStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        ITestPaperMapper iTestPaperMapper = (ITestPaperMapper) Tools.getImp("ITestPaperMapper");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //添加单选题请求
        String add_single_choice = request.getParameter("query_all_SingleChoice");
        if (add_single_choice != null) {
            SingleChoice[] singleChoices = iTestPaperMapper.queryAllSingleChoice();
            out.write(JSON.toJSONString(singleChoices));
            out.flush();
            out.close();
            return;
        }

        //添加单选题请求
        String del_single_choice = request.getParameter("del_single_choice");
        if (del_single_choice != null) {
            iTestPaperMapper.delSingleChoice(Integer.parseInt(del_single_choice));
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加多选题请求
        String query_all_multiple_choice_question = request.getParameter("query_all_multiple_choice_question");
        if (query_all_multiple_choice_question != null) {
            MultipleChoiceQuestion[] mcq = iTestPaperMapper.queryAllMultipleChoiceQuestion();
            out.write(JSON.toJSONString(mcq));
            out.flush();
            out.close();
            return;
        }

        //添加多选题请求
        String del_multiple_choice_question = request.getParameter("del_multiple_choice_question");
        if (del_multiple_choice_question != null) {
            iTestPaperMapper.delMultipleChoiceQuestion(Integer.parseInt(del_multiple_choice_question));
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加判断选题请求
        String query_all_judge_question = request.getParameter("query_all_judge_question");
        if (query_all_judge_question != null) {
            JudgeQuestion[] jq = iTestPaperMapper.queryAllJudgeQuestion();
            out.write(JSON.toJSONString(jq));
            out.flush();
            out.close();
            return;
        }

        //添加判断选题请求
        String del_judge_question = request.getParameter("del_judge_question");
        if (del_judge_question != null) {
            iTestPaperMapper.delJudgeQuestion(Integer.parseInt(del_judge_question));
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加填空题请求
        String query_all_gap_filling = request.getParameter("query_all_gap_filling");
        if (query_all_gap_filling != null) {
            GapFilling[] gf = iTestPaperMapper.queryAllGapFilling();
            out.write(JSON.toJSONString(gf));
            out.flush();
            out.close();
            return;
        }

        //添加填空题请求
        String del_gap_filling = request.getParameter("del_gap_filling");
        if (del_gap_filling != null) {
            iTestPaperMapper.delGapFilling(Integer.parseInt(del_gap_filling));
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加简答题请求
        String query_all_essay_question = request.getParameter("query_all_essay_question");
        if (query_all_essay_question != null) {
            EssayQuestion[] eq = iTestPaperMapper.queryAllEssayQuestion();
            out.write(JSON.toJSONString(eq));
            out.flush();
            out.close();
            return;
        }

        //添加简答请求
        String del_essay_question = request.getParameter("del_essay_question");
        if (del_essay_question != null) {
            iTestPaperMapper.delEssayQuestion(Integer.parseInt(del_essay_question));
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //返回所有题目数量
        String query_all_topic = request.getParameter("query_all_topic");
        if (query_all_topic != null) {
            out.write(TestPaperMapperImp.queryAllTopic());
            out.flush();
            out.close();
            return;
        }

        //返回所有题目数量
        String query_all_Student_test_paper = request.getParameter("query_all_Student_test_paper");
        if (query_all_Student_test_paper != null) {
            Student_test_paper[] stp = iTestPaperMapper.query_all_Student_test_paper(Integer.parseInt(query_all_Student_test_paper));
            if (stp.length == 0) {
                out.write("null");
                out.flush();
                out.close();
            }

            String[] resultSet = new String[stp.length];
            IStudentMapper iStudentMapper = (IStudentMapper) Tools.getImp("IStudentMapper");
            for (int index = 0, len = stp.length; index < len; index++) {
                resultSet[index] = "" + stp[index].getTpId() + "###" + stp[index].getStuId()
                        + "###" + iStudentMapper.findStuId(stp[index].getStuId()).getStuName() + "###" + stp[index].getGrade();
            }

            out.write(JSON.toJSONString(resultSet));
            out.flush();
            out.close();
            return;
        }

        //返回当前时间
        String query_current_time = request.getParameter("query_current_time");
        if (query_current_time != null) {
            //发送当前
            out.write("" + new Date().getTime());
            out.flush();
            out.close();
            return;
        }

        //返回考试时间
        System.out.println("路过------------------------------------------------------------");
        String query_testPaper_time = request.getParameter("query_testPaper_time");
        if (query_testPaper_time != null) {
            System.out.println("-----------------------------------------------------------");
            TestPaper curTestPaper = iTestPaperMapper.getCurTestPaper();
            String time = curTestPaper.getStartTime().getTime() + "#####" + curTestPaper.getEntTime().getTime();
            //发送考试的时间
            System.out.println("返回的考试时间:" + curTestPaper.getStartTime().getTime() + "  " + curTestPaper.getEntTime().getTime());
            out.write(time);
            out.flush();
            out.close();
            return;
        }

        //返回考试试卷
        String query_testPaper_node = request.getParameter("query_testPaper_node");
        if (query_testPaper_node != null) {
            TestPaper testPaper = iTestPaperMapper.getCurTestPaper();
            Date curTime = new Date();

            if (curTime.getTime() < testPaper.getStartTime().getTime()) {
                out.write("no_beginning");
                out.flush();
                out.close();
                return;
            } else if (curTime.getTime() > testPaper.getEntTime().getTime()) {
                out.write("already_over");
                out.flush();
                out.close();
                return;
            }

            TestPaper curTestPaper = iTestPaperMapper.getCurTestPaper();
            Location_table lt = new Location_table(curStudent.getStuId(), curTestPaper.getId(), getIpAddr(request));

            //查询该考生是否在其它设备上已经登录
            Location_table location_table = iTestPaperMapper.query_location_table(lt);
            if (location_table != null) {
                if (!location_table.getIp().equals(getIpAddr(request))) {
                    out.write("other_location###" + location_table.getIp());
                    out.flush();
                    out.close();
                    return;
                }
            } else {
                //第一次登录，添加进地址判定表
                iTestPaperMapper.addLocation_table(new Location_table(curStudent.getStuId(), curTestPaper.getId(), getIpAddr(request)));
            }

            Student_test_paper stp = new Student_test_paper();
            //添加学生编号
            stp.setStuId(curStudent.getStuId());

            //添加试卷编号
            stp.setTpId(testPaper.getId());

            //添加考试内容
            stp.setNode(TestPaperMapperImp.createTestPaperJson(getIpAddr(request), curTestPaper.getNode()));

            //发送考试的时间
            out.write(JSON.toJSONString(stp));
            out.flush();
            out.close();
            return;
        }

        //添加考试信息
        String add_topic = request.getParameter("add_topic");
        if (add_topic != null) {
            TestPaper testPaper = new TestPaper();
            String[] result = add_topic.split("@@@@");
            String ip = getIpAddr(request);
            testPaper.setNode(result[0]);
            String[] timeSet = result[1].split("AAA");
            java.util.Date startDate = new java.util.Date();

            //startTime[0]:开始的小时  startTime[0]:开始的分钟
            String[] startTime = timeSet[0].split("--");
            startDate.setHours(Integer.parseInt(startTime[0]));
            startDate.setMinutes(Integer.parseInt(startTime[1]));
            startDate.setSeconds(0);

            testPaper.setStartTime(startDate);

            //endTime[0]:开始的小时  endTime[0]:开始的分钟
            String[] endTime = timeSet[1].split("--");
            Date endDate = new Date();
            endDate.setHours(Integer.parseInt(endTime[0]));
            endDate.setMinutes(Integer.parseInt(endTime[1]));
            endDate.setSeconds(0);
            testPaper.setEntTime(endDate);

            iTestPaperMapper.addTestPaper(testPaper);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加学生试卷信息
        String add_student_topic = request.getParameter("add_student_topic");
        if (add_student_topic != null) {
            Student_test_paper stp = JSON.parseObject(add_student_topic, Student_test_paper.class);
            Student_test_paper[] student_test_paper = iTestPaperMapper.query_student_test_paper_stuId_and_tpId(stp.getStuId(), stp.getTpId());
            if (student_test_paper != null && student_test_paper.length >= 1) {
                out.write("be_defeated");
                out.flush();
                out.close();
                return;
            }

            iTestPaperMapper.addStudent_test_paper(stp);
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加学生试卷信息
        String query_student_topic_t = request.getParameter("query_student_topic_t");
        if (query_student_topic_t != null) {
            String[] split = query_student_topic_t.split("###");
            //试卷id###学生id
            Student_test_paper[] student_test_papers = iTestPaperMapper.query_student_topic_t(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
            if (student_test_papers == null || student_test_papers.length == 0) {
                out.write("OK");
                out.flush();
                out.close();
                System.out.println("可以答卷!");
                return;
            }
            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //添加考试信息
        String set_testPaper_grate = request.getParameter("set_testPaper_grate");
        if (set_testPaper_grate != null) {
            String[] strSet = set_testPaper_grate.split("###");
            TestPaperGrate[] testPaperGrates = iTestPaperMapper.queryTestPaperGrate(Integer.parseInt(strSet[0]));
            if (testPaperGrates != null && testPaperGrates.length >= 1) {
                iTestPaperMapper.update_test_paper_grate(new TestPaperGrate(Integer.parseInt(strSet[0]),
                        Integer.parseInt(strSet[1]), Integer.parseInt(strSet[2]), Integer.parseInt(strSet[3]), Integer.parseInt(strSet[4]), Integer.parseInt(strSet[5])));
            } else {
                iTestPaperMapper.add_test_paper_grate(new TestPaperGrate(Integer.parseInt(strSet[0]),
                        Integer.parseInt(strSet[1]), Integer.parseInt(strSet[2]), Integer.parseInt(strSet[3]), Integer.parseInt(strSet[4]), Integer.parseInt(strSet[5])));
            }

            out.write("succeed");
            out.flush();
            out.close();
            return;
        }

        //返回各题的分数
        String queryTestPaperGrate = request.getParameter("queryTestPaperGrate");
        if (queryTestPaperGrate != null) {
            TestPaperGrate[] testPaperGrates = iTestPaperMapper.queryTestPaperGrate(Integer.parseInt(queryTestPaperGrate));

            if (testPaperGrates == null || testPaperGrates.length == 0) {
                out.write("failure");
                out.flush();
                out.close();
                return;
            }

            //返回该考生的试卷
            out.write(JSON.toJSONString(testPaperGrates[0]));
            out.flush();
            out.close();
            return;
        }

        //根据试卷编号和学生编号查询学生考试信息
        String query_studentTestPaper = request.getParameter("query_studentTestPaper");
        if (query_studentTestPaper != null) {
            String[] strSet = query_studentTestPaper.split("###");
            System.out.println("收到的信息是:" + query_studentTestPaper);
            Student_test_paper[] stp = iTestPaperMapper.query_student_test_paper_stuId_and_tpId(Integer.parseInt(strSet[1]), Integer.parseInt(strSet[0]));
            if (stp == null || stp.length == 0) {
                out.write("not_found");
                out.flush();
                out.close();
                return;
            }

            //返回该考生的试卷
            out.write(JSON.toJSONString(stp[0]));
            out.flush();
            out.close();
            return;
        }

        //返回当前试卷的id
        String query_curTestPaperId = request.getParameter("query_curTestPaperId");
        if (query_curTestPaperId != null) {
            if (iTestPaperMapper.getCurTestPaper().getId() == null) {
                out.write("not_found");
                out.flush();
                out.close();
                return;
            }

            //返回当前试卷的id
            out.write("" + iTestPaperMapper.getCurTestPaper().getId());
            out.flush();
            out.close();
            return;
        }

        //添加试卷标题
        String add_TP_title = request.getParameter("add_TP_title");
        if (add_TP_title != null) {
            String[] nodeSer = add_TP_title.split("###");
            iTestPaperMapper.add_TP_title(new TP_Title(Integer.parseInt(nodeSer[0]), nodeSer[1], nodeSer[2]));
            out.write("OK");
            out.flush();
            out.close();
            return;
        }

        //查询试卷标题
        String query_TP_title = request.getParameter("query_TP_title");
        if (query_TP_title != null) {
            TP_Title tp_title = iTestPaperMapper.query_TP_title(Integer.parseInt(query_TP_title));
            out.write(tp_title.getHeadline() + "###" + tp_title.getSubtitle());
            out.flush();
            out.close();
            return;
        }

        //查询试卷标题
        String set_tpGrade = request.getParameter("set_tpGrade");
        if (set_tpGrade != null) {
            String[] nodes = set_tpGrade.split("###");
            System.out.println(Arrays.toString(nodes));
            iTestPaperMapper.set_TP_grade(Integer.parseInt(nodes[0]), Integer.parseInt(nodes[1]), Integer.parseInt(nodes[2]));
            out.write("OK");
            out.flush();
            out.close();
            return;
        }

        //查询试卷标题
        String get_curTPId = request.getParameter("get_curTPId");
        if (get_curTPId != null) {
            TestPaper curTestPaper = iTestPaperMapper.getCurTestPaper();
            if (curTestPaper == null) {
                out.write("empty");
                out.flush();
                out.close();
                return;
            }

            out.write("" + curTestPaper.getId());
            out.flush();
            out.close();
            return;
        }

        //查询试卷标题
        String query_not_correct_TP = request.getParameter("query_not_correct_TP");
        if (query_not_correct_TP != null) {
            Student_test_paper[] student_test_papers = iTestPaperMapper.query_not_correct_TP(Integer.parseInt(query_not_correct_TP));
            if (student_test_papers.length == 0) {
                out.write("empty");
                out.flush();
                out.close();
                return;
            }

            out.write("" + student_test_papers[0].getStuId());
            out.flush();
            out.close();
            return;
        }

        //查询每场的试卷信息
        String query_all_tpSet = request.getParameter("query_all_tpSet");
        if (query_all_tpSet != null) {
            TestPaperSet[] testPaperSets = iTestPaperMapper.query_all_tpSet();
            if (testPaperSets == null || testPaperSets.length == 0) {
                out.write("null");
                out.flush();
                out.close();
                return;
            }

            for (TestPaperSet testPaperSet : testPaperSets) {
                if (iTestPaperMapper.tp_whether_check_accomplish(testPaperSet.getTpId()) > 0) {
                    testPaperSet.setStatus(true);
                }
            }

            TestPaperSet[] iTestPaperMapperCopy = new TestPaperSet[testPaperSets.length];
            int curIndex = 0;
            for (TestPaperSet testPaperSet : testPaperSets) {
                if (testPaperSet.getStatus()) {
                    iTestPaperMapperCopy[curIndex++] = testPaperSet;
                }
            }

            for (TestPaperSet testPaperSet : testPaperSets) {
                if (!testPaperSet.getStatus()) {
                    iTestPaperMapperCopy[curIndex++] = testPaperSet;
                }
            }

            out.write(JSON.toJSONString(iTestPaperMapperCopy));
            out.flush();
            out.close();
            return;
        }

        out.write("fail");
        out.flush();
        out.close();
    }

    public String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip.equals("0:0:0:0:0:0:0:1")) {
            return "192.168.32.3";
        }
        return ip;
    }
}
