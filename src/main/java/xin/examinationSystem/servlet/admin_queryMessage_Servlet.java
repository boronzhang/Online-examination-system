package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.dao.ITeacherMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/admin_queryMessage_Servlet")
public class admin_queryMessage_Servlet extends HttpServlet {
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean adminStatus = false;
        boolean teacherStatus = false;
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessage")) {
                HttpSession session = request.getSession();
                Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    adminStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!adminStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }

        //查询所有学生信息--------------------------------------------------------------------------------
        String studentMessage = request.getParameter("queryAllStudentMessage");
        if (studentMessage != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            Student[] allStudent = iAdminMapper.findAllStudent();
            String students = JSON.toJSONString(allStudent);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(students);
            out.flush();
            out.close();
            return;

        }

        //判断是否添加的是教师信息----------------------------------------------------------------------------
        String teacherMessage = request.getParameter("queryAllTeacherMessage");
        if (teacherMessage != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            Teacher[] allTeacher = iAdminMapper.findAllTeacher();
            String teachers = JSON.toJSONString(allTeacher);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(teachers);
            out.flush();
            out.close();
            return;

        }

        String queryStudentSum = request.getParameter("queryStudentSum");
        if (queryStudentSum != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(iAdminMapper.queryStudentSum());
            out.flush();
            out.close();
            return;

        }

        String queryTeacherSum = request.getParameter("queryTeacherSum");
        if (queryTeacherSum != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(iAdminMapper.queryTeacherSum());
            out.flush();
            out.close();
            return;

        }

        String queryTeacherIdSum = request.getParameter("queryTeacherIdSum");
        if (queryTeacherIdSum != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(JSON.toJSONString(iAdminMapper.queryTeacherIdSum()));
            out.flush();
            out.close();
            return;

        }

        String queryStudentIdSum = request.getParameter("queryStudentIdSum");
        if (queryStudentIdSum != null) {

            IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(JSON.toJSONString(iAdminMapper.queryStudentIdSum()));
            out.flush();
            out.close();
            return;
        }
    }

    public void destroy() {
    }
}