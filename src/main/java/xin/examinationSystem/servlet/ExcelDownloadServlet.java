package xin.examinationSystem.servlet;

import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Teacher;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

@WebServlet("/downloadExcel")
public class ExcelDownloadServlet extends HttpServlet {

    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean adminStatus = false;
        boolean teacherStatus = false;
        Cookie[] cookies = request.getCookies();
        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessage")) {
                HttpSession session = request.getSession();
                Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    adminStatus = true;
                }
            }
        }

        for (Cookie tCookie : cookies) {
            if (tCookie.getName().equals("accountMessageTeacher")) {
                HttpSession session = request.getSession();
                Teacher attribute = (Teacher) session.getAttribute(tCookie.getValue());
                if (attribute != null) {
                    teacherStatus = true;
                }
            }
        }


        if (!adminStatus && !teacherStatus) {
            response.sendRedirect("/index.html");
            return;
        }


        //获取要下载的文件名
        String downloadFileName = "";
        //A1:单选题模板,A2:多选题模板,A3:判断题模板,A4:填空题模板,A5:简答题模板
        String downloadExcelCode = request.getQueryString();

        switch (downloadExcelCode) {
            case "A1":
                downloadFileName = "单选题模板.xlsx";
                break;
            case "A2":
                downloadFileName = "多选题模板.xlsx";
                break;
            case "A3":
                downloadFileName = "判断题模板.xlsx";
                break;
            case "A4":
                downloadFileName = "填空题模板.xlsx";
                break;
            case "A5":
                downloadFileName = "简答题模板.xlsx";
                break;
        }

        //读取要下载的文件内容
        ServletContext servletContext = getServletContext();

        String mimeType = servletContext.getMimeType("/Files/" + downloadFileName);
        //在回传前,通过响应头告诉客户端返回的数据类型
        response.setContentType(mimeType);

        //获取浏览器不能用utf-8,要用base64
        //还要告诉客户端收到的数据是用于下载使用的(还是使用响应头),URLEncoder.encode()把汉字按指定的格式转为%xx的形式
        response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(downloadFileName, "UTF-8"));

        //获取资源的输入流,服务器解析的地址为http://ip/ 映射到web目录
        InputStream resourceAsStream = servletContext.getResourceAsStream("/Files/" + downloadFileName);

        //获取相应的输出流
        OutputStream outputStream = response.getOutputStream();

        //读取输入流中全部的数据,复制给给输入流,把下载的内容回传给客户端
        IOUtils.copy(resourceAsStream, outputStream);
    }

    public void destroy() {
    }
}