package xin.examinationSystem.servlet;

import com.alibaba.fastjson.JSON;
import xin.examinationSystem.account.Admin;
import xin.examinationSystem.account.Student;
import xin.examinationSystem.account.Teacher;
import xin.examinationSystem.dao.IAdminMapper;
import xin.examinationSystem.dao.IStudentMapper;
import xin.examinationSystem.utils.Tools;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;

@WebServlet("/admin_logIn_Servlet")
public class admin_logIn_Servlet extends HttpServlet {

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String str = request.getParameter("admAutoLogIn");
        //如果客户端存在cookie
        if (str != null && str.equals("YES")) {
            Cookie[] cookies = request.getCookies();
            for (Cookie tCookie : cookies) {
                if (tCookie.getName().equals("accountMessage")) {
                    HttpSession session = request.getSession();
                    Admin attribute = (Admin) session.getAttribute(tCookie.getValue());
                    if (attribute != null) {
                        response.setContentType("text/html;charset=UTF-8");
                        PrintWriter out = response.getWriter();
                        out.write("/html/adminHome.html");
                        out.flush();
                        out.close();
                        return;
                    }
                }
            }
            PrintWriter out = response.getWriter();
            out.write("NoAutoLogin");
            out.flush();
            out.close();
            return;
        }

        //获取参数
        String content = request.getParameter("adminJson");

        //JSON转对象
        Admin admin = JSON.parseObject(content, Admin.class);

        //获取操作接口
        IAdminMapper iAdminMapper = (IAdminMapper) Tools.getImp("IAdminMapper");
        Admin query = iAdminMapper.findAccount(admin.getAdmAccount());
        if (query == null) {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("AccountDoesNotExist");
            out.flush();
            out.close();
            return;
        } else if (!query.getAdmPassword().equals(admin.getAdmPassword())) {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write("PasswordError");
            out.flush();
            out.close();
            return;
        }

        //发送要跳转的页面的url
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.write("/html/adminHome.html");

        //根据时间产生的用户标识
        String userId = "" + (new Date().getTime());
        System.out.println("产生的用户id:" + userId);

        //cookie存储key:accountMessage,value:根据时间毫秒数产生的id
        Cookie cookie = new Cookie("accountMessage", userId);
        cookie.setPath("/");
        //在六个月内有效
        cookie.setMaxAge(60 * 60 * 24 * 30 * 6);
        response.addCookie(cookie);

        //把时间产生的用户id设置为key,账户信息设置为value
        HttpSession session = request.getSession();
        session.setAttribute(userId, query);

        out.flush();
        out.close();
    }

    public void destroy() {
    }
}